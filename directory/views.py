# coding: utf-8

from django.shortcuts import get_object_or_404

# NOTE: render() из mezzanine использует lazy rendering. Это важно, поскольку
#       нам нужна переменная page в контексте шаблона, а эта переменная
#       попадает в контекст через PageMiddleware уже после того, как отработает
#       наша view.
from mezzanine.utils.views import render

from directory.models import Boat, Yachtsman


def fleet(request):
    boats = Boat.objects.select_related('city').all() \
                        .order_by('city__id', 'name')
    return render(request, 'directory/fleet.html', {'boats': boats})


def boat_detail(request, pk):
    boat = get_object_or_404(Boat, pk=pk)
    return render(request, 'directory/boat_detail.html', {'boat': boat})


def yachtsmen(request):
    yachtsmen = Yachtsman.objects.select_related('city').all() \
                         .order_by('city__id', 'name')
    return render(request, 'directory/yachtsmen.html', {'yachtsmen': yachtsmen})


def yachtsman_detail(request, pk):
    yachtsman = get_object_or_404(Yachtsman, pk=pk)
    return render(request, 'directory/yachtsman_detail.html', {'yachtsman': yachtsman})

