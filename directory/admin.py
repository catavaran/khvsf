# coding: utf-8
from django.contrib import admin

from directory.models import City, Boat, BoatImage, Yachtsman, YachtsmanImage


class CityAdmin(admin.ModelAdmin):
    pass    

admin.site.register(City, CityAdmin)


class BoatImageInline(admin.TabularInline):
    model = BoatImage
    extra = 3


class BoatAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'design', )
    list_filter = ('city', )
    inlines = [BoatImageInline]

admin.site.register(Boat, BoatAdmin)


class YachtsmanImageInline(admin.TabularInline):
    model = YachtsmanImage
    extra = 3


class YachtsmanAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', )
    list_filter = ('city', )
    inlines = [YachtsmanImageInline]
    
admin.site.register(Yachtsman, YachtsmanAdmin)
