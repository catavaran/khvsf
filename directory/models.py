# coding: utf-8
from django.db import models

from mezzanine.core.fields import RichTextField


class City(models.Model):

    name = models.CharField(u'наименование', max_length=20)

    class Meta:
        verbose_name = u'город'
        verbose_name_plural = u'города'

    def __unicode__(self):
        return self.name


class Boat(models.Model):

    city = models.ForeignKey(City, verbose_name=u'город')
    name = models.CharField(u'название', max_length=30)
    num = models.CharField(u'номер', max_length=10, blank=True, null=True)
    design = models.CharField(u'модель', max_length=30, blank=True, null=True)
    length = models.DecimalField(u'длина', max_digits=10,
                                 decimal_places=2, blank=True, null=True)
    year = models.IntegerField(u'год выпуска', blank=True, null=True)
    description = RichTextField(u'описание', null=True, blank=True)

    class Meta:
        verbose_name = u'яхта'
        verbose_name_plural = u'яхты'

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('boat_detail', [str(self.id)])

    def first_image(self):
        return self.images.all().first()


class BoatImage(models.Model):

    def upload_path(self, filename):
        return u'fleet/%s/%s' % (self.boat.id, filename)

    boat = models.ForeignKey(Boat, verbose_name=u'яхта', related_name='images')
    image = models.ImageField(u'фото', upload_to=upload_path, max_length=200)
    description = models.TextField(u'описание', null=True, blank=True)

    class Meta:
        verbose_name = u'фото яхты'
        verbose_name_plural = u'фото яхт'


class Yachtsman(models.Model):

    city = models.ForeignKey(City, verbose_name=u'город')
    name = models.CharField(u'ФИО', max_length=50)
    description = RichTextField(u'описание', null=True, blank=True)

    class Meta:
        verbose_name = u'яхтсмен'
        verbose_name_plural = u'яхтсмены'

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('yachtsman_detail', [str(self.id)])
        
    def first_image(self):
        return self.images.all().first()


class YachtsmanImage(models.Model):

    def upload_path(self, filename):
        return u'yachtsman/%s/%s' % (self.yachtsman.id, filename)

    yachtsman = models.ForeignKey(Yachtsman, verbose_name=u'яхтсмен', related_name='images')
    image = models.ImageField(u'фото', upload_to=upload_path, max_length=200)
    description = models.TextField(u'описание', null=True, blank=True)

    class Meta:
        verbose_name = u'фото яхтсмена'
        verbose_name_plural = u'фото яхтсменов'
