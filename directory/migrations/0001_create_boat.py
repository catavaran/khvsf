# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'City'
        db.create_table(u'directory_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'directory', ['City'])

        # Adding model 'Boat'
        db.create_table(u'directory_boat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.City'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('num', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('design', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('year', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('description', self.gf('mezzanine.core.fields.RichTextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['Boat'])

        # Adding model 'BoatImage'
        db.create_table(u'directory_boatimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('boat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Boat'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['BoatImage'])


    def backwards(self, orm):
        # Deleting model 'City'
        db.delete_table(u'directory_city')

        # Deleting model 'Boat'
        db.delete_table(u'directory_boat')

        # Deleting model 'BoatImage'
        db.delete_table(u'directory_boatimage')


    models = {
        u'directory.boat': {
            'Meta': {'object_name': 'Boat'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.City']"}),
            'description': ('mezzanine.core.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'design': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.boatimage': {
            'Meta': {'object_name': 'BoatImage'},
            'boat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Boat']"}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '200'})
        },
        u'directory.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['directory']