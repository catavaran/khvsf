from django.conf.urls import patterns, url

urlpatterns = patterns('directory.views',

    url('^fleet/$', 'fleet', name='fleet'),
    url('^fleet/(\d+)/$', 'boat_detail', name='boat_detail'),
    url('^yachtsmen/$', 'yachtsmen', name='yachtsmen'),
    url('^yachtsmen/(\d+)/$', 'yachtsman_detail', name='yachtsman_detail'),

)
